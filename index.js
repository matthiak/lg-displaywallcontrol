let express = require('express');
let auth = require('http-auth');
let Serial = require('serialport');
let path = require('path');
let argv = require('minimist')(process.argv.slice(2));

let DEBUG = Boolean(argv.d);
let comport = argv.p ? argv.p : 'COM4';

if (argv.h) {
	console.log(`
LG display wall control server

Starts a http server on port 3000. Users can go to
the site and control tiling mode of the screens.

Parameters:
	-d Debugging mode. (For development)
	-p <PORT> The COM port (In Windows e.g. COM3)
		default COM port is COM4
	`);

	return;
}

console.log('debug', DEBUG, 'comport', comport);

let com;

let lgcommands = {
	tiles: {
		left: {
			screens: [
				1, 2, 3, 6, 7, 8, 'b', 'c', 'd'
			],
			mode: '33'	//3x3 tile
		},
		top: {
			screens: [
				4, 5, 9, 'a'
			],
			mode: '22'	//2x2 tile
		},
		bottom: {
			screens: [
				'e', 'f'
			],
			mode: '21'	//2x1 tile (column x row)
		},
	}
}

let keepalivebuffer = new Array();
let cmdbuffer = new Array();

function sendCmd(cmd, screen, data) {
	let str = cmd + ' ' + screen + ' ' + data + '\n';
	//console.log('sending', str);
	if (!DEBUG && com && com.write)
		com.write(str);
	else {
		//console.error('com.send not working');
	}
}

lgcommands.tiles.left.screens.forEach((i, idx) => {
	// simulate remote control 'info'-button  to keep screens alive
	keepalivebuffer.push({ cmd: 'mc', screen: i, data: 'AA' });
});

lgcommands.tiles.top.screens.forEach((i, idx) => {
	// simulate remote control 'info'-button  to keep screens alive
	keepalivebuffer.push({ cmd: 'mc', screen: i, data: 'AA' });
});

lgcommands.tiles.bottom.screens.forEach((i, idx) => {
	// simulate remote control 'info'-button  to keep screens alive
	keepalivebuffer.push({ cmd: 'mc', screen: i, data: 'AA' });
});
let itv;
let running = false;
function send(singleshot) {
	let index = 0;
	let lengthKeepAlive = keepalivebuffer.length;
	let swap = false;
	
	// prevent multiple calls of send..
	// successive calls will just add to the cmdbuffer
	if (!running) {
		running = true;
		itv = setInterval(() => {

			//swap between sending command and
			//keep alive (show info)
			if (!swap) {
				let cmd = cmdbuffer.shift();
				if (cmd)
					sendCmd(cmd.cmd, cmd.screen, cmd.data);
				swap = true;
				process.stdout.write(cmdbuffer.length.toString());
			} else {
				let i = index % lengthKeepAlive;
				let cmd = keepalivebuffer[i];
				sendCmd(cmd.cmd, cmd.screen, cmd.data);
				process.stdout.write('.');
				index++;
				swap = false;
			}
			if (singleshot && cmdbuffer.length == 0) {
				clearInterval(itv);
				running = false;
			}
		}, 180);
	}

}
function stopLoop() {
	clearInterval(itv);
	running = false;
}
/*
	setup serial connetion
*/
let input = '';
if (!DEBUG) {
	com = new Serial(comport, { baudRate: 9600 });

	com.on('error', (err) => {
		console.log('serial conn open error: ', err.message);
	});

	com.on('data', (data) => {
		input += data.toString();
		let idx = input.indexOf('x');
		if(idx > 0)
			console.log('response: ', input.substring(0, idx));
		if(idx < input.length)
			input = input.substring(idx + 1);
		else
			input = '';
	});
}
let app = express();

let basic = auth.basic({
	realm:"Control area",
	file: __dirname + "/.htaccess"
})

// order of app.use is important

// 1. user authentication
app.use(auth.connect(basic));
// 2. serve html docs from public folder
app.use(express.static(path.join(__dirname, 'public')));


app.get('/cmd/:cmd/screen/:screen/:data', function (req, res) {
	res.send('hello there' + JSON.stringify(req.params));
	cmdbuffer.push({ cmd: req.params.cmd, screen: req.params.screen, data: req.params.data });
	send(true);
});

app.get('/tile/:pos/:onoff', function (req, res) {
	res.send('tiling' + JSON.stringify(req.params));
	if (req.params.onoff == 'on') {
		lgcommands.tiles[req.params.pos].screens.forEach((i, idx) => {
			cmdbuffer.push({ cmd: 'dd', screen: i.toString(), data: lgcommands.tiles[req.params.pos].mode }); // tile mode 3x3
			//cmdbuffer.push({ cmd: 'di', screen: i.toString(), data: idx + 1 }); // tile ID
			cmdbuffer.push({ cmd: 'xb', screen: i.toString(), data: '70' });// input DVI
		});
	} else {
		lgcommands.tiles[req.params.pos].screens.forEach((i, idx) => {
			cmdbuffer.push({ cmd: 'dd', screen: i.toString(), data: '0' }); // tile mode off
			cmdbuffer.push({ cmd: 'xb', screen: i.toString(), data: '90' }); // input HDMI

		});
	}
	send();
});

app.get('/settileid/:pos/', function (req, res) {
	res.send('settileid' + JSON.stringify(req.params));
	lgcommands.tiles[req.params.pos].screens.forEach((i, idx) => {
		cmdbuffer.push({ cmd: 'di', screen: i.toString(), data: idx + 1 }); // tile mode 3x3
	});
});

app.get('/stopinfo', function (req, res) {
		res.send('stopping info screen');
		stopLoop();
	});
app.get('/showinfo', function (req, res) {
		res.send('starting info screen');
		send(false);
	});

app.get('/mutescreens/:onoff', function(req, res) {
	res.send('muting screens' + req.params.onoff);
	let command = { cmd: 'kd', screen: 0, data: req.params.onoff }
	console.log(command.cmd, command.screen, command.data)
	cmdbuffer.push(command);
/*
	for(let tile of Object.keys(lgcommands.tiles)) {
		lgcommands.tiles[tile].screens.forEach((i, idx) => {
			let command = { cmd: 'kd', screen: i.toString(), data: req.params.onoff }
			console.log(command.cmd, command.screen, command.data)
			cmdbuffer.push(command); 
		});
	}
*/
	send(true); //single shot send
});
	
app.get('/input/:input', function (req, res) {
	let data = 'ff';
	if (input == 'hdmi') {
		data ='90';
	} else if (input == 'dvi') {
		data ='70';
	}
	cmdbuffer.push({ cmd: 'xb', screen: 0, data: data });
	send();
});

app.listen(3000, () => {
	console.log('started');
});

console.log('here');