function clicked(cmd) {

	let url;
	switch (cmd) {
		case 'tilelefton':
			url = "tile/left/on";
			break;
		case 'tileleftoff':
			url = "tile/left/off";
			break;
		case 'tiletopon':
			url = "tile/top/on";
			break;
		case 'tiletopoff':
			url = "tile/top/off";
			break;		
		case 'tilebottomon':
			url = "tile/bottom/on";
			break;
		case 'tilebottomoff':
			url = "tile/bottom/off";
			break;			
		case 'mutescreens':
			url = "mutescreens/1";
			break;		
		case 'unmutescreens':
			url = "mutescreens/0";
			break;		
		default:
			url = cmd;
			break;

	}

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById('status').innerText = "Response: " + this.responseText;
			setTimeout(() => {
				document.getElementById('status').innerText = "";
			}, 2000);
		}
	}
	xhttp.open("GET", url, true);
	xhttp.send();
}